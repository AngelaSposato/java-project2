package spellingbee.network;

import spellingbee.server.*;

/**
 * This class will run on the server side and is used to connect the server code to the backend business code.
 * This class is where the "protocol" will be defined.
 */
public class ServerController {
	// This is the interface you will be creating!
	// Since we are storing the object as an interface, you can use any type of ISpellingBeeGame object.
	// This means you can work with either the SpellingBeeGame OR SimpleSpellingBeeGame objects and can
	// seamlessly change between the two.
	private SpellingBeeGame spellingBee = new SpellingBeeGame();

	/**
	 * Action is the method where the protocol translation takes place.
	 * This method is called every single time that the client sends a request to the server.
	 * It takes as input a String which is the request coming from the client. 
	 * It then does some actions on the server (using the ISpellingBeeGame object)
	 * and returns a String representing the message to send to the client
	 * @param inputLine The String from the client
	 * @return The String to return to the client
	 */
	public String action(String inputLine) {
		/* Your code goes here!!!!
		 * Note: If you want to preserve information between method calls, then you MUST
		 * store it into private fields.
		 * You should use the spellingBee object to make various calls and here is where your communication
		 * code/protocol should go.*/
		if (inputLine.equals("getCenter")) {
			// client has requested getCenter. Call the getCenter method which returns a String of 7 letters
			return Character.toString(spellingBee.getCenterLetter());
		}
		else if (inputLine.equals("getAllLetters")) {
			return spellingBee.getAllLetters();
		}
		else if (inputLine.equals("getScore")) {
			return ""+spellingBee.getScore();
		}
		else if (inputLine.equals("getBrackets")) {
			return spellingBee.arrayToString();
		}
		else if(inputLine.startsWith("checkValid")){
			//initializing end result variable and the number of points
			String result = "";
			String s1;
			int s2;
			//splitting the inputLine to separate the word
			String[] input = inputLine.split(";");
			//calling getMessage to get the result(Good! or Bad)
			s1 = spellingBee.getMessage(input[1]);
			//if the the answer is good, it will search for the number of points
			if(s1.equals("Good!")) {
			    s2 = spellingBee.getPointsForWord(input[1]);
			}
			//otherwise the user gets no points for that round
			else if(s1.equals("Bad!")){
				s2 = 0;
			}
			//if the word has already been used it sets the message and the number of points to 0
			else {
				s1 = "This word was already used";
				s2 = 0;
			}
			//joining the two strings to send back to the event handler (with a ; so I can separate the message and the points)
			result = s1+";"+s2;
			return result;
		}
		return null;
	}
}
