package spellingbee.client;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import spellingbee.network.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

/**
 * A class created to generate the user interface.
 * @author Angela, Christian
 */
public class SpellingBeeClient extends Application {
	private Client newClient = new Client(); 
	
	public static void main(String[] args) {
		Application.launch(args);
	}
	/**
	 * Generates user interface
	 * @author Angela, Christian
	 */
	public void start(Stage stage) {
		Group root = new Group();
		Scene scene = new Scene(root, 650, 400);
		scene.setFill(Color.WHITE);
		TabPane tabPane = new TabPane();
		ScoreTab scores = new ScoreTab(this.newClient);
		GameTab game = new GameTab(this.newClient);
		TextField scoreField = game.getScoreField();
		//Adding eventListener that calls refresh when change detected in GameTab
		scoreField.textProperty().addListener((observableValue, oldValue, newValue) -> {
			scores.refresh();
		});
		tabPane.getTabs().add(scores);
		tabPane.getTabs().add(game);
		root.getChildren().add(tabPane);
		stage.setTitle("Test");
		stage.setScene(scene);
		stage.show();
	}
}
