package spellingbee.client;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import spellingbee.network.Client;

/**
 * @author Cristian
 * this class creates the layout of the GameTab and sets the eventHandlers for the buttons
 *
 */
public class GameTab extends Tab{
	private EventHandlers e;
	private Client c;
	TextField t1;
	TextField t3;
	
/**
 * @author Cristian
 * @param client
 * constructor calss super to get the name of the tab, sets the client object and runs generateContent();
 */
public GameTab(Client client) {
	   super("Game");
	   this.c = client;
	   generateContent();
   }


/**
 * @author Cristian
 * Generates the contents of the Game tab and adds event listeners
 * 
 */
public void generateContent(){
	//creating grid pane that will store all the elements
	   GridPane grid = new GridPane();
	   
	   //creating first hbox that will store the letter buttons
	   HBox h1 = new HBox();
	   
	   // array allLettersA will hold all the letters
	   String[] allLettersA = c.sendAndWaitMessage("getAllLetters").split("");
	   
	   //creating all the buttons and setting the text to a letter
	   Button b1 = new Button(allLettersA[0]);
	   //button 1 will always be the center letter, hence making it red
	   b1.setTextFill(Color.RED);
	   
	   Button b2 = new Button(allLettersA[1]);
	   Button b3 = new Button(allLettersA[2]);
	   Button b4 = new Button(allLettersA[3]);
	   Button b5 = new Button(allLettersA[4]);
	   Button b6 = new Button(allLettersA[5]);
	   Button b7 = new Button(allLettersA[6]);
	   
	   //t1 will hold the answer of the player
	   t1 = new TextField();
	   
	   //buttons array will hold the buttons so I can use a loop to automatically give them an event listener
	   Button[] buttons = { b1,b2,b3,b4,b5,b6,b7};
	   for(Button b : buttons) {
		   e = new EventHandlers(t1,b.getText(),"letter");
		   b.setOnAction(e);
	   }
	   
	   //adding all buttons the the first hbox and spacing out items
	   h1.setSpacing(5);
	   h1.getChildren().addAll(b1,b2,b3,b4,b5,b6,b7);
	   
	   //t2 is the field that will show the outcome of a round
	   TextField t2 = new TextField("Good!");
	   t2.setPrefWidth(180);
	   //t3 is the field that will show how many points that round has given
	   t3 = new TextField("Points");
	   
	   //h2 is the hbox where the submit,delete and backspace buttons will be
	   HBox h2 = new HBox();
	   
	   //creating the submit button, its object and adding event listener
	   Button submit = new Button("Submit");
	   e = new EventHandlers(t1,t2,t3,"submit",c);
	   submit.setOnAction(e);
	   
	 //creating the delete button, its object and adding event listener
	   Button del = new Button("Delete");
	   e = new EventHandlers(t1,"delete");
	   del.setOnAction(e);
	   
	 //creating the backspace button, its object and adding event listener
	   Button backSpace = new Button("BackSpace");
	   e = new EventHandlers(t1,"backspace");
	   backSpace.setOnAction(e);
	   
	   //adding submit,backspace and delete button to h2
	   h2.getChildren().addAll(submit,del,backSpace);
	   
	   //h3 is the hbox that will hold the outcome and the points fields of the round
	   HBox h3 = new HBox();
	   
	   //adding the outcome and point fields of the player
	   h3.setSpacing(5);
	   h3.getChildren().addAll(t2,t3);
	   
	   //adding elements to the grid
	   grid.add(h1,0,0);
	   grid.add(t1, 0, 1);
	   grid.add(h2, 0, 2);
	   grid.add(h3, 0, 3);
	   
	   //setting the grid as the game tab content
	   this.setContent(grid);	   
   }

/**
 * @Author Cristian
 * @return t3
 * method returns the scoreField
 */
public TextField getScoreField() {
	   return t3;
   }
}
