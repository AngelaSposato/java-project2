package spellingbee.client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import spellingbee.network.Client;

/**
 * @author Cristian
 * this class will create different objects depending on button, and handle the events they must do
 *
 */
public class EventHandlers implements EventHandler<ActionEvent>{
	//initializing constructor variables
	private TextField tf;
    private TextField tOutcome;
    private TextField tPoints;
    private String letter;
    private String handler;
    private Client c;
    
    //Constructor for the letter buttons
    public EventHandlers(TextField tf,String letter, String handler) {
    	this.tf = tf;
    	this.letter = letter;
    	this.handler = handler;
    }
    
    //Constructor for the submit button
    public EventHandlers(TextField tf,TextField tOutcome,TextField tPoints,String handler, Client c) {
    	this.tf = tf;
    	this.tOutcome = tOutcome;
    	this.tPoints = tPoints;
    	this.handler = handler;
    	this.c = c;
    }
    
    //Constructor for the delete and backspace button
    public EventHandlers(TextField tf,String handler) {
    	this.tf = tf;
    	this.handler = handler;
    }
	/**
	 * @author Cristian
	 * this method handles the different buttons according to their handler value
	 */
	@Override
	public void handle(ActionEvent Event) {
		//adds the letter button to the answer textField
		if(handler.equals("letter")) {
			tf.setText(tf.getText()+ this.letter);
		}
		//calls the server with the word attempt and sets the outcome and points textField to it's response
		else if(handler.equals("submit")) {
			String[] result = c.sendAndWaitMessage("checkValid;"+tf.getText()).split(";");
			tOutcome.setText(result[0]);
			tPoints.setText(result[1]);
			//clearing text after round so that the player doesn't have to delete every time
			tf.setText("");
		}
		//empties the answer textField
		else if(handler.equals("delete")) {
			tf.setText("");
		}
		//takes out 1 character of the answer textField
		else if(handler.equals("backspace")) {
			int endIndex = tf.getText().length() - 1;
			tf.setText(tf.getText().substring(0,endIndex));
		}
	}
}
