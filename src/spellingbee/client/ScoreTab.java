package spellingbee.client;

import spellingbee.server.*;
import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;

import javafx.scene.layout.HBox;
import spellingbee.network.Client;

/**
 * This class generates a Tab that keeps track of the user's score
 * @author Angela
 */
public class ScoreTab extends Tab{
	Text queen;
	Text genius;
	Text amazing;
	Text good; 
	Text gettingStarted;
	Text currentScore;
	Text queenNumber;
	Text geniusNumber;
	Text amazingNumber;
	Text goodNumber;
	Text okNumber;
	Text scoreField;
	private Client client;
	
	public ScoreTab(Client client) {
		super("Score");
		this.client = client;
		GridPane grid = generatePane();
		this.setContent(grid);
	}
	
	/**
	 * This method calls the other methods to generate a GridPane with information about the score.
	 * @author Angela
	 * @return grid 
	 */
	public GridPane generatePane() {
		GridPane grid = new GridPane();
		setTextCategories();
		addTextToGrid(grid);
		setTextColor();
		setGridSpacing(grid);
		generateBracketNumbers();
		addNumbersToGrid(grid);
		return grid;
	}
	/**
	 * Helper method that initializes text fields for categories
	 * @author Angela
	 */
	private void setTextCategories() {
		queen = new Text("Queen Bee");
		genius = new Text("Genius");
		amazing = new Text("Amazing");
		good = new Text("Good");
		gettingStarted = new Text("Getting Started");
		currentScore = new Text("Current Score: ");
	}
	
	/**
	 * Helper method that adds Text fields to grid
	 * @author Angela
	 * @param grid
	 */
	private void addTextToGrid(GridPane grid) {
		grid.add(queen, 0, 0);
		grid.add(genius, 0, 1);
		grid.add(amazing, 0, 2);
		grid.add(good, 0, 3);
		grid.add(gettingStarted, 0, 4);	
		grid.add(currentScore, 0, 5);
	}
	
	/**
	 * Helper method that sets the color of the Text Fields
	 * @author Angela
	 */
	private void setTextColor() {
		queen.setFill(Color.GREY);
		genius.setFill(Color.GREY);
		amazing.setFill(Color.GREY);
		good.setFill(Color.GREY);
		gettingStarted.setFill(Color.GREY);
		currentScore.setFill(Color.RED);
	}
	
	/**
	 * Helper method that makes a call to Server for info on brackets and stores this information in Text fields
	 * @author Angela
	 */
	private void generateBracketNumbers() {
		String[] brackets = client.sendAndWaitMessage("getBrackets").split(" ", -1);
		String score = client.sendAndWaitMessage("getScore");
		queenNumber = new Text(brackets[4]);
		geniusNumber = new Text(brackets[3]);
		amazingNumber = new Text(brackets[2]);
		goodNumber = new Text(brackets[1]);
		okNumber = new Text(brackets[0]);
		scoreField = new Text(score);
	}
	
	/**
	 * Helper method that adds Text fields to GridPane
	 * @author Angela
	 * @param grid GridPane for score
	 */
	private void addNumbersToGrid(GridPane grid) {
		grid.add(queenNumber, 1, 0);
		grid.add(geniusNumber, 1, 1);
		grid.add(amazingNumber, 1, 2);
		grid.add(goodNumber, 1, 3);
		grid.add(okNumber, 1, 4);
		grid.add(scoreField, 1, 5);
	}
	/**
	 * Helper method that sets Vgap and Hgap of grid to 10
	 * @author Angela
	 * @param grid GridPane for score
	 */
	private void setGridSpacing(GridPane grid) {
		grid.setVgap(10);
		grid.setHgap(10);
	}
	
	/**
	 * Updates scoreField and colours every time there is a change
	 * @author Angela
	 */
	public void refresh() {
		String[] brackets = client.sendAndWaitMessage("getBrackets").split(" ", -1);
		int scoreNumber = Integer.parseInt(client.sendAndWaitMessage("getScore"));
		String scoreString = client.sendAndWaitMessage("getScore");
		int okBracket = Integer.parseInt(brackets[0]);
		int goodBracket = Integer.parseInt(brackets[1]);
		int amazingBracket = Integer.parseInt(brackets[2]);
		int geniusBracket = Integer.parseInt(brackets[3]);
		int queenBracket = Integer.parseInt(brackets[4]);
		scoreField.setText(scoreString);
		if (scoreNumber >= okBracket) {
			gettingStarted.setFill(Color.BLACK);
		}
		else if (scoreNumber >= goodBracket) {
			gettingStarted.setFill(Color.BLACK);
			good.setFill(Color.BLACK);
		}
		else if (scoreNumber >= amazingBracket) {
			gettingStarted.setFill(Color.BLACK);
			good.setFill(Color.BLACK);
			amazing.setFill(Color.BLACK);
		}
		else if (scoreNumber >= geniusBracket) {
			gettingStarted.setFill(Color.BLACK);
			good.setFill(Color.BLACK);
			amazing.setFill(Color.BLACK);
			genius.setFill(Color.BLACK);
		}
		else if (scoreNumber >= queenBracket) {
			gettingStarted.setFill(Color.BLACK);
			good.setFill(Color.BLACK);
			amazing.setFill(Color.BLACK);
			genius.setFill(Color.BLACK);
			queen.setFill(Color.BLACK);
		}
	}
}
