package spellingbee.server;

import java.util.*;
import java.io.IOException;
//import java.nio.*;
import java.nio.file.*;

/**
 * This class contains all the logic for the Spelling Bee Game. 
 * @author Angela
 */

public class SpellingBeeGame implements ISpellingBeeGame{
	String letters;
	String centerLetter;
	int currentScore;
	int[] brackets;
	HashSet<String> wordsFound;
	public static HashSet<String> possibleWords = new HashSet<String>();
	
	private final static int MIN_LENGTH = 4;
	private final static int BONUS_POINTS = 7;
	private final static int LETTER_SET_LENGTH = 7; 
	private final static int MIN_LENGTH_POINTS = 1;
	private final static int INVALID_WORD_POINTS = 0;
	private final static double PERCENT_25 = 0.25;
	private final static double PERCENT_50 = 0.5;
	private final static double PERCENT_75 = 0.75;
	private final static double PERCENT_90 = 0.9;
	
	public SpellingBeeGame() {
		letters = chooseAllLetters();
		centerLetter = chooseCenterLetter();
		possibleWords = createWordsFromFile("english.txt");
		currentScore = 0;
		wordsFound = new HashSet<String>();
		brackets = getBrackets();
	}
	public SpellingBeeGame(String letters) {
		this.letters = letters;	
		centerLetter = chooseCenterLetter();
		possibleWords = createWordsFromFile("english.txt");
		currentScore = 0;
		wordsFound = new HashSet<String>();
	}

	/**
	 * This method adds lines from a file to a HashSet, goes through each line in the List, 
	 * checks for the 3 main rules:
	 * 1. word must be at least 4 letters
	 * 2. center letter must be included
	 * 3. only letters from letter set must be included
	 * @author Angela
	 * @param path
	 * @return HashSet<String> containing all the valid words from the list
	 * @throws IOException
	 */
	private HashSet<String> createWordsFromFile(String path) {
		HashSet<String> allWords = new HashSet<String>();
		HashSet<String> validWords = new HashSet<String>();
		try {
			Path p = Paths.get(path);
			allWords.addAll(Files.readAllLines(p));
			//For each String s in HashSet lines
			for (String word: allWords) {
				//Checking if center letter is included and that length is minimum 4
				if (word.indexOf(centerLetter) >= 0 && word.length() >= MIN_LENGTH) {
					//Counter keeps tracks of how many letter set are included in word
					int counter = 0; 
					for (int i = 0; i<word.length(); i++) {
						//If it is greater than 0, means that letter is in word
						if (letters.indexOf(word.charAt(i)) >= 0) {
							counter++;
						}
					}
					//Assuring that only words with letters from the letter set are being added to validWords
					if (counter == word.length()) {
						validWords.add(word);
					}
				}
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return validWords;
	}

	/**
	 * Calculates maximum number of points possible for a word, returns array of brackets with percentages of points
	 * @author Angela
	 * @return int[] with brackets
	 */
	public int[] getBrackets() {
		int numPoints = 0;
		for (String s: possibleWords) {
			// if pangram
			if (checkPangram(s) == true) {
				numPoints = numPoints + s.length() + BONUS_POINTS;
			}
			//equal to 4 and center letter is included
			else if (s.length() == MIN_LENGTH && s.indexOf(centerLetter) >= 0) {
				numPoints = numPoints + MIN_LENGTH_POINTS;
			}
			//greater than 4 and center letter is included
			else if (s.length() > MIN_LENGTH && s.indexOf(centerLetter) >= 0) {
				numPoints = numPoints + s.length();
			}
			//no points added if center letter is not included (no points added)
			else if (s.indexOf(centerLetter) >= 0) {
				continue;
			}			
		}
		int value25 = (int) (numPoints * PERCENT_25);
		int value50 = (int) (numPoints * PERCENT_50);
		int value75 = (int) (numPoints * PERCENT_75);
		int value90 = (int) (numPoints * PERCENT_90);
		int value100 = numPoints;
		brackets = new int[] {value25, value50, value75, value90, value100};
		return brackets;
	}

	/**
	 * Takes as input the attempt and returns the number of points the user earned for their attempt.
	 * @author Angela
	 * @param attempt: String containing user's attempt
	 * @return amount of points for the word
	 */
	public int getPointsForWord(String attempt) {
		if (checkPangram(attempt) == true) {
			return attempt.length() + BONUS_POINTS;
		}
		else if (attempt.length() == MIN_LENGTH) {
			return MIN_LENGTH_POINTS;
		}
		else if (attempt.length() > MIN_LENGTH) {
			return attempt.length();
		}
		//If it does not fall under those cateogories, return 0
		else {
			return INVALID_WORD_POINTS;
		}
	}
	/**
	 * This is a helper method that checks if there is a pangram from user input
	 * @author Angela
	 * @param attempt - String with user input
	 * @return boolean of whether user input is a pangram or not
	 */
	private boolean checkPangram(String attempt) {
		int letterCounter = 0;
		//goes through each letter in letter set
		for (int i = 0; i<letters.length(); i++) {
			//for every letter from letters that attempt contains, letterCounter is incremented
			if (attempt.indexOf(letters.charAt(i)) >= 0) {
				letterCounter++;
			}
		}
		//if the counter is equal 7 all letters from letter set have been included and should return true
		if (letterCounter == LETTER_SET_LENGTH) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * This method checks if the list of possible words 
	 * (which has been filtered in createWordsFromFile based on the rules of the Spelling Bee game)
	 * contains the user's attempt.
	 * @author Angela
	 * @param String attempt: user's input
	 * @return String message: depending on whether input was valid or not
	 */
	public String getMessage(String attempt) {
		//making sure it has not been already found and that it is part of possible words
		if (!(wordsFound.contains(attempt))) {
			if (possibleWords.contains(attempt)) {
				//add to words found
				wordsFound.add(attempt);
				//add to current score
				currentScore = currentScore + getPointsForWord(attempt);
				return "Good!";
			}
			else {
				return "Bad!";
			}
		}
		//if word already found
		else {
			return "Word already found!";
		}
	}

	/**
	 * Returns all the letters in the letter set as a String
	 * @author Angela
	 * @return String containing all letters from set
	 */
	public String getAllLetters() {
		return letters;
	}

	/**
	 * Returns chosen center letter by taking String centerLetter
	 * and calling the charAt on the index 0 (first letter) to convert it to a char and return it
	 * @author Angela
	 * @return char containing chosen center letter
	 */
	public char getCenterLetter() {
		char center = centerLetter.charAt(0);
		return center; 
	}

	/**
	 * Returns player's score
	 * @author Angela
	 * @return int representing user's current score
	 */
	public int getScore() { 
		return currentScore;
	}

	/**
	 * This is a helper method that chooses the center letter from the letter set.
	 * @author Angela
	 * @return String containing the center letter
	 */
	private String chooseCenterLetter() {
		char center = letters.charAt(0);
		centerLetter = Character.toString(center);
		return centerLetter;
	}

	/**
	 * Helper method that chooses a letter combination from the text file
	 * @author Angela
	 * @return String containing letter combination 
	 */
	private String chooseAllLetters() {
		Random randGen = new Random();
		try {
			Path p = Paths.get("letterCombinations.txt");
			List<String> lines = Files.readAllLines(p);
			int index = randGen.nextInt(lines.size());
			letters = lines.get(index);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return letters;
	}
	/**
	 * Helper method that turns array into a String 
	 * @author Angela
	 * @return String containing all values of brackets[] array separated by a space.
	 */
	public String arrayToString() {
		String bracketString = 
				brackets[0] + " " +
				brackets[1] + " " +
				brackets[2] + " " +
				brackets[3] + " " +
				brackets[4] + " " ;
		return bracketString;				
	}
	
}
