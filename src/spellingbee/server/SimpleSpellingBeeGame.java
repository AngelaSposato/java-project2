package spellingbee.server;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;

/**
 * @author Cristian
 * SimpleSpellingBeeGame is used to create letters, count outcomes and manipulate results
 *
 */
public class SimpleSpellingBeeGame implements ISpellingBeeGame{
	//Creating variables used for the constructor
	private int currentScore;
	private int[] brackets;
	private String letters;
	private String centerLetter;
	private String letter2;
	private String letter3;
	private String letter4;
	private String letter5;
	private String letter6;
	private String letter7;
	public static HashSet<String> possibleWords = new HashSet<String>();
	HashSet<String> wordsFound;
	
	
	/**
	 * @author Cristian
	 * sets hard-coded letters, current score, creates the possibleWords hashSet, brackets and creates wordFound
	 */
	public SimpleSpellingBeeGame() {
		currentScore = 0;
		centerLetter = "o";
		letter2 = "a";
		letter3 = "l";
		letter4 = "d";
		letter5 = "b";
		letter6 = "r";
		letter7 = "n";
		letters = "oaldbrn";
		possibleWords = addPossibleWords();
		brackets = getBrackets();
		wordsFound = new HashSet<String>();
		
	}

	/**
	 * @author Cristian
	 * Calculates the points for word attempted
	 *
	 */
	@Override
	
	public int getPointsForWord(String attempt) {
		if(checkPangram(attempt) == true){
			return attempt.length() + 7;
		}
	    else if(attempt.length() < 4) {
			return 0;
		}
		else if(attempt.length() == 4) {
			return 4;
		}
		else if(attempt.length() > 4) {
			return attempt.length();
		}
		return 0;
	}

	/**
	 * @author Cristian
	 * sends "Good" or "Bad" depending on the word attempted
	 */
	@Override
	public String getMessage(String attempt) {
		//if the word is not already found, it will continue
		if(!wordsFound.contains(attempt)) {
			//if the word is good, it will it will add the word to the words found, add the points of the word to the score and return good
			if(possibleWords.contains(attempt)) {
				wordsFound.add(attempt);
				currentScore = currentScore + getPointsForWord(attempt);
				return "Good!";
			}
			else {
				return "Bad!";
			}
		}
		else {
			return "You already tried this word";
		}
	}

	/**
	 * @author Cristian
	 * returns all the letters
	 */
	@Override
	public String getAllLetters() {
		return letters;
	}

	/**
	 * @author Cristian
	 * returns the center letter
	 */
	@Override	
	public char getCenterLetter() {
		char center = centerLetter.charAt(0);
		return center; 
	}

	/**
	 * @author Cristian
	 * returns the current score
	 */
	@Override
	public int getScore() {
		return currentScore;
	}

	/** 
	 * @author Cristian
	 * calculates the brackets, by counting the points each word will get
	 */
	@Override
	public int[] getBrackets() {
		int points = 0;
		for (String word: possibleWords) {
			if (checkPangram(word) == true) {
				points = points + word.length() + 7;
			}
			else if (word.length() == 4) {
				points = points + 1;
			}
			else if (word.length() > 4) {
				points = points + word.length();
			}
		}
		int bracket25 = (int) (points * 0.25);
		int bracket50 = (int) (points * 0.5);
		int bracket75 = (int) (points * 0.75);
		int bracket90 = (int) (points * 0.9);
		int bracket100 = points;
		brackets = new int[] {bracket25, bracket50, bracket75, bracket90, bracket100};
		return brackets;
	}
	
	
	/**
	 * @author Cristian
	 * @param attempt
	 * @return boolean
	 * 
	 * checks if the current word is a pangram or not
	 */
	public boolean checkPangram(String attempt) {
		int letterCounter = 0;
		for (int i = 0; i<letters.length(); i++) {
			//look into this later	
			if (attempt.indexOf(attempt.charAt(i)) != -1) {
				letterCounter++;
			}
		}
		if (letterCounter == 7) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * @author Cristian
	 * @return words
	 * 
	 * adds the hard-coded words to the hashSet possibleWords
	 */
	public HashSet<String> addPossibleWords(){
		HashSet<String> words = new HashSet<String>();
		words.add("abandon");
		words.add("adorn");
		words.add("bandrol");
		words.add("bard");
		return words;
	}
}
